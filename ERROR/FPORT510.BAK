/*
  FPORT510.C -- Main startup and support routines for
  FPORT/510 NLM driver


  Copyright (C) 1993 MurkWorks

*/

#include	"fportdef.h"
#include	<conio.h>
#include	<string.h>
#include	<signal.h>
#include	<errno.h>

/* globals */
FPortInfo	Fports[MAX_FPORTS];
static	char	*fpga_initfile = {"sys:system/fport"};

/* the FPGA load file (fport) could be embedded in the NLM as a custom
   file, and loaded into memory during the initial NLM load phase. This
   would allow a single file to be distributed with the package.

   downsides -- requires re-linking to update the FPORT array, and
		12K of memory would always be in use by this data
*/

#ifdef	COMMENTS
Here's what we expect on the command line.

load fport510 <args>

Where <args> are:
	port [=] ioaddress_hex			[278, 280, 290, 378]
	int[errupt] [=] int_decimal		[5,7,10,11,12,15]
	q[ueue] [=] queue_to_service
	pserv[er] [=] print_server_login_name
	pass[word] [=] print_server_password
	max[throughput] = kbytes_per_second_decimal  [0, 25-1250]
	fpga = path			(default sys:system/fport)
example:

	load fport510 port 3f8 int=5 q=blatque pserv=fport max=0

#endif


void
Usage()
{
	char	*usage[]={
		"FPORT510 NLM Driver Usage\r\n",
		"load fport510 <args>\r\n",
		"    where <args> is one or more of the following:\r\n",
		"    port = <portnum>        hex I/O Port Address\r\n",
		"    interrupt = <int>       decimal hardware interrupt value\r\n",
		"    queue = <queue name>    Print Queue Name to service\r\n",
		"    pserver = <print server name>  Print Server name to login as\r\n",
		"    password = <print server password> Print Server password if not null\r\n",
		"    maxthroughput = <maximum kbytes/sec>  Maximum throughput in kbytes/second\r\n",
		"    fpga = <path to fpga file>  (optional)\r\n",
		"\r\n",
		"    portnum must be [278, 280, 290, 378]\r\n",
		"    interrupt must be [5, 7, 10, 11, 12, 15]\r\n",
		"    maxthroughput must be [0 (unlimited, default), 25 - 1250]\r\n",
		"\r\n",
		"example:\r\n",
		"load fport510 port=387 int=11 q=fqueue pserv=fport max=500\r\n",
		NULL
	};
	int	x;

	for(x=0; usage[x]; x++)
		ConsolePrintf("%s",usage[x]);
}

void
Int0_Handler(void)
{
	FPServicePort(&Fports[0]);

	if(Fports[0].fi_doeoi)  {
		if(Fports[0].fi_interrupt > 7)
			outp(0xa0, 0x20);
		outp(0x20,0x20);
	}
}

void
Int1_Handler(void)
{
	FPServicePort(&Fports[1]);
	if(Fports[1].fi_doeoi) {
		if(Fports[1].fi_interrupt > 7)
			outp(0xa0, 0x20);
		outp(0x20,0x20);
	}
}

void
Int2_Handler(void)
{
	FPServicePort(&Fports[2]);
	if(Fports[2].fi_doeoi) {
		if(Fports[2].fi_interrupt > 7)
			outp(0xa0, 0x20);
		outp(0x20,0x20);
	}
}

void
Int3_Handler(void)
{
	FPServicePort(&Fports[3]);
	if(Fports[3].fi_doeoi) {
		if(Fports[3].fi_interrupt > 7)
			outp(0xa0, 0x20);
		outp(0x20,0x20);
	}
}

int
Configure(int fport, int port, int intval, int maxthp, char *queue, long oidq,
	   char *pserv, long oidp, char *pass, char *fpga)
{
	int	x;
	LONG	resourcetag;

	/* partially configure the fport struct and call the Init function
	   to see if the port exists */

	Fports[fport].fi_port = port;
	Fports[fport].fi_interrupt = intval;
	switch(fport) {
		case 0 :
			Fports[0].fi_intsrv = Int0_Handler;
			break;
		case 1 :
			Fports[1].fi_intsrv = Int1_Handler;
			break;
		case 2 :
			Fports[2].fi_intsrv = Int2_Handler;
			break;
		case 3 :
			Fports[3].fi_intsrv = Int3_Handler;
			break;
		default :
			ConsolePrintf("FPORT Internal error, port value too large\r\n");
			return(1);
	}


	x = FPInitializeFPGA(&Fports[fport], fpga);
	switch(x) {
		case FPINIT_RC_OK :	/* no error */
			break;
		case FPINIT_RC_NO_SUCH_FILE :
			ConsolePrintf("FPORT Could not find FPGA load file (%s)\r\n",fpga);
			Usage();
			return(1);
		case FPINIT_RC_BAD_FILE :
			ConsolePrintf("FPORT Incorrect or Invalid FPGA file specified (%s)\r\n",fpga);
			Usage();
			return(1);
		case FPINIT_RC_BAD_INIT :
			ConsolePrintf("FPORT FPORT510 card failed to initialize\r\n");
			Usage();
			return(1);
		case FPINIT_RC_NO_CARD :
			ConsolePrintf("FPORT FPORT510 card not found at specified port (%x)\r\n",port);
			Usage();
			return(1);
		case FPINIT_RC_FIFO_ERROR :
			ConsolePrintf("FPORT FPORT510 fifo readback error\r\n");
			return(1);
		default :
			ConsolePrintf("FPORT Unknown Initialization error %d\r\n",x);
			return(1);
	}
	/* do final configuration */

	/* #1, get a resource tag for the hardware int handler */
	resourcetag = AllocateResourceTag( GetNLMHandle(), "FPORT510 Handler", HardwareInterruptSignature);
	if(!resourcetag) {	/* aigh! */
		ConsolePrintf("FPORT Couldn't allocate resource tag!\r\n");
		return(1);
	}

	/* #2 mark the port as in use */
	Fports[fport].fi_status = FPS_IDLE;
	Fports[fport].fi_qstatus = FPS_IDLE;
	Fports[fport].fi_queuename = strdup(queue);
	Fports[fport].fi_pservername = strdup(pserv);
	Fports[fport].fi_password = strdup(pass);
	Fports[fport].fi_queueoid = oidq;
	Fports[fport].fi_maxthp	= maxthp;
	Fports[fport].fi_cachereadsize = DEFAULT_ASYNCREAD_SIZE;
	Fports[fport].fi_polldelay = DEFAULT_QUEUE_POLL;

	/* #3 link into hardware int vector */
	_disable();
	x =  SetHardwareInterrupt( intval,
				       Fports[fport].fi_intsrv,
				       resourcetag,
				       (BYTE)0,
				       (BYTE)0,
                                       &Fports[fport].fi_doeoi);
	_enable();

	if(x) {
		ConsolePrintf("FPORT Internal error 0x%x, could not set hardint handler\r\n",x);
		Fports[fport].fi_status = FPS_NONE;
		free(Fports[fport].fi_queuename);
		free(Fports[fport].fi_pservername);
		free(Fports[fport].fi_password);
		/* should free resource tag, but there's no way to do it */
		return(1);
	}

	/* #4 allocate cache blocks */
	if(FPAllocCacheBlocks(&Fports[fport], DEFAULT_CACHEBLOCK_COUNT)) {
		ConsolePrintf("FPORT Internal Error, could not allocate cache blocks\r\n");
		Fports[fport].fi_status = FPS_NONE;
		free(Fports[fport].fi_queuename);
		free(Fports[fport].fi_pservername);
		free(Fports[fport].fi_password);
		_disable();
		ClearHardwareInterrupt( Fports[fport].fi_interrupt, Fports[fport].fi_intsrv);
		_enable();
		/* should free resource tag, but there's no way to do it */
		return(1);
	}
	switch(Fports[fport].fi_interrupt) {
		int	mask;
		case 5 :
		case 7 :
			/* on first controller */
			mask = inp(0x21);
			mask &= ~(1 << Fports[fport].fi_interrupt);
			outp(0x21, mask);
			break;
		default :;
			/* on second controller */
			mask = inp(0xa1);
			mask &= ~(1 << (Fports[fport].fi_interrupt - 8));
			outp(0xa1, mask);
			break;
	}
	/* #5 start a threadgroup to handle this port */
	x = BeginThreadGroup(FPServiceThread, NULL, SERVICE_STACK_SIZE, &Fports[fport]);
	if(x == EFAILURE) {
		ConsolePrintf("FPORT Couldn't spawn service thread, error 0x%x/0x%x\r\n",x,errno);
		Fports[fport].fi_status = FPS_NONE;
		free(Fports[fport].fi_queuename);
		free(Fports[fport].fi_pservername);
		free(Fports[fport].fi_password);
		FPShutdown(&Fports[fport]);
		FPFreeCacheBlocks(&Fports[fport]);
		_disable();
		ClearHardwareInterrupt( Fports[fport].fi_interrupt, Fports[fport].fi_intsrv);
		_enable();
		return(1);
	}
#if	DEBUGX
	ConsolePrintf("FPORT info port=0x%x int=0x%x doeoi %d\r\n",
		Fports[fport].fi_port, Fports[fport].fi_interrupt,
		Fports[fport].fi_doeoi);
#endif
	return(0);
}

int
ProcessCommandLine(char *cl)
/* process the command line and carry out instructions
   all errors are reported to the console

   returns  0 - no error
	    1 - error

  ** Assumes executed from within critsec handler **
*/
{
	struct	_keywords {
		char	*keyword;
		int	minlength;
	} KeyWords[] = {
		{"port",4},
		{"interrupt",3},
		{"queue",1},
		{"pserver",5},
		{"password",4},
		{"maxthroughput",3},
		{"fpga",4},
		{NULL,0}
	};

	char	*cm = cl;
	char	*keyword,  *arg;
static char	*tokens={" =\t"};
	int	rc = 1;
	int	fport;
	int	x;

	int	port 	= 0;
	int	intval 	= 0;
	int	maxthp	= 0;
	char 	*queue 	= NULL;
	long	oidq	= 0;
	char	*pserv	= NULL;
	long	oidp	= 0;
	char	*pass	= NULL;
	char	*fpga	= NULL;

	/* make sure that not all ports are configured */
	for(fport = 0; fport < MAX_FPORTS; fport++) {
		if(Fports[fport].fi_status == FPS_NONE)
			break;
	}
	if(fport == MAX_FPORTS) {
		ConsolePrintf("FPORT All ports are currently in use\r\n");
		return(1);
	}
	keyword = strtok(cm, tokens);
	while(keyword) {
		int	kw;
		int	argval;
		int	len = strlen(keyword);

		arg = strtok(NULL, tokens);
		if(!arg) {
			ConsolePrintf("FPORT Missing argument for keyword %s\r\n",keyword);
			Usage();
			return(1);
		}
		/* parse keyword/arg */
		for(kw=0; KeyWords[kw].minlength; kw++) {
			if(!strnicmp(KeyWords[kw].keyword, keyword,
				len > KeyWords[kw].minlength ? KeyWords[kw].minlength : len))
					break;
		}
		if(!KeyWords[kw].minlength) {	/* unknown keyword */
			ConsolePrintf("FPORT Unknown keyword (%s)\r\n",keyword);
			Usage();
			return(1);
		}
		switch(kw) {
			case 0 :		/* port */
				if((sscanf(arg, "%x", &argval) != 1) ||
					(argval != 0x278 &&
					 argval != 0x280 &&
					 argval != 0x290 &&
					 argval != 0x378)) {

					ConsolePrintf("FPORT Invalid port value (%x)\r\n",argval);
					Usage();
					return(1);
				}
				port = argval;

				break;
			case 1 :		/* int */
				if((sscanf(arg, "%d", &argval) != 1) ||
					(argval != 5 &&
					 argval != 7 &&
					 argval != 10 &&
					 argval != 11 &&
					 argval != 12 &&
					 argval != 15)) {

					ConsolePrintf("FPORT Invalid interrupt value (%d)\r\n",argval);
					Usage();
					return(1);
				}
				intval = argval;

				break;
			case 2 :		/* queue */
				/* make sure this queue exists */
				strupr(arg);
				if(GetBinderyObjectID(arg, OT_PRINT_QUEUE, &oidq)) {
					ConsolePrintf("FPORT Print Queue %s does not exist\r\n",arg);
					Usage();
					return(1);
				}
				queue = arg;
				break;
			case 3 :		/* pserver */
				strupr(arg);
				if(strchr(arg,'/')) {
					ConsolePrintf("FPORT NLM can only service local queues\r\n");
					Usage();
					return(1);
				}
				if(GetBinderyObjectID(arg, OT_PRINT_SERVER, &oidp)) {
					ConsolePrintf("FPORT Print Server %s does not exist\r\n",arg);
					Usage();
					return(1);
				}
				pserv = arg;
				break;
			case 4 :		/* password */
				strupr(arg);
				pass = arg;
				break;
			case 5 :		/* max throughput */
				if((sscanf(arg, "%d", &argval) != 1) ||
					(argval != 0 &&
					 (argval < 5 ||
					 argval > 1250))) {
					ConsolePrintf("FPORT Invalid maxthroughput value (%d) must be between 5 and 1250 or 0\r\n",argval);
					Usage();
					return(1);
				}
				maxthp = argval;
				break;
			case 6 :		/* fpga */
				fpga = arg;
				break;
			default :;		/* error */
		}	/* end switch */
		/* get next keyword */
		keyword = strtok(NULL, tokens);
	}	/* end while */
	/* get here, verify print server password, if set */
	if(!port)
		port = DEFAULT_PORT;

	/* check to see if this port is already configured */
	for(x = 0; x < MAX_FPORTS; x++) {
		if(Fports[x].fi_status != FPS_NONE &&
		   Fports[x].fi_port == port) {	/* already configured */
		   /* later, we might allow attachment to a new queue... */
		   ConsolePrintf("FPORT port %x already configured, unload nlm to reassign\r\n",port);
		   return(1);
		}
	}

	if(!intval)
		intval = DEFAULT_INTERRUPT;

	/* check to see if this int is already configured on another port */
	for(x = 0; x < MAX_FPORTS; x++) {
		if(Fports[x].fi_status != FPS_NONE &&
		   Fports[x].fi_interrupt == intval) {	/* already configured */
		   ConsolePrintf("FPORT interrupt %d already configured on port %x\r\n",intval,
			Fports[x].fi_port);
		   return(1);
		}
	}

	if(!fpga)
		fpga = fpga_initfile;

	if(!pserv || !queue) {
		ConsolePrintf("FPORT Both a queue and pserver must be specified\r\n");
		Usage();
		return(1);
	}

	if(!pass)
		pass = "";

	if(VerifyBinderyObjectPassword(pserv, OT_PRINT_SERVER, pass)) {
		ConsolePrintf("FPORT Incorrect print server password specified\r\n");
		Usage();
		return(1);
	}

	/* final check, is oidp in oidq 's Q_SERVERS list */
	rc = IsBinderyObjectInSet(queue, OT_PRINT_QUEUE, "Q_SERVERS", pserv, OT_PRINT_SERVER);
	if(rc) {
		ConsolePrintf("FPORT PrintServer %s is not a Q_SERVER for queue %s\r\n",pserv, queue);
		Usage();
		return(1);
	}
	rc = Configure(fport, port, intval, maxthp, queue, oidq,
		   pserv, oidp, pass, fpga);

	return(rc);
}

void
SigtermHandler(int sigtype)
/* make sure all threads shutdown, then release hd ints */
{
	int	fport;
	long	endtime = time(NULL) + MAX_SIGTERM_WAIT;

	for(fport=0; fport < MAX_FPORTS; fport++)
		Fports[fport].fi_shutdown = 1;

	ConsolePrintf("FPORT Please wait while service threads are shut down\r\n");
	while(time(NULL) < endtime) {
		for(fport=0; fport < MAX_FPORTS; fport++) {
			if(Fports[fport].fi_status == FPS_BUSY ||
			   (Fports[fport].fi_qstatus != FPS_DONE &&
			    Fports[fport].fi_qstatus != FPS_NONE))
				break;
		}
		if(fport < MAX_FPORTS) {	/* someone was busy */
			ConsolePrintf(".");
			delay(5000);		/* wait 5 seconds and try again */
			continue;
		}
		/* get here, everyone is done */
		ConsolePrintf("\r\nDone\r\n");
		return;
	} /* end while */
	ConsolePrintf("\r\nFPORT Warning, could not shut down all threads\r\n");
}

int
CheckIfBusy()		/* registered in linker */
{
	int	fport;

	for(fport = 0; fport < MAX_FPORTS; fport++) {
		if(Fports[fport].fi_status == FPS_BUSY ||
		   Fports[fport].fi_qstatus == FPS_BUSY)
			break;
	}
	if(fport == MAX_FPORTS)
		return(0);
	ConsolePrintf("FPORT NLM is currently active\r\n");
	return(1);
}

void
UnloadHandler()
{
	/* free all strdup'd values, and release hardware int handler */
	int	fport;
ConsolePrintf("in UNload\r\n");
	for(fport=0; fport < MAX_FPORTS; fport++) {
		if(Fports[fport].fi_status == FPS_NONE)
			continue;

ConsolePrintf("Unload queuefree\r\n");
		free(Fports[fport].fi_queuename);
ConsolePrintf("Unload pserv free\r\n");
		free(Fports[fport].fi_pservername);
ConsolePrintf("Unload pass free\r\n");
		free(Fports[fport].fi_password);

		_disable();
		ClearHardwareInterrupt( Fports[fport].fi_interrupt, Fports[fport].fi_intsrv);
		_enable();

ConsolePrintf("Unload cach free\r\n");
		FPFreeCacheBlocks(&Fports[fport]);
ConsolePrintf("done cache free\r\n");

		Fports[fport].fi_status = FPS_NONE;
	}
ConsolePrintf("Unload done\r\n");
}


int
main()
{
	char	*cm = strdup(getcmd(NULL));
	long	nexttime;

	EnterCritSec();		/* inhibit any other threads from running */
	if(ProcessCommandLine(cm)) {
		ExitCritSec();
		free(cm);
		return(1);
	}
	/* setup unloadprocedures, sigterm handler, etc */
	free(cm);

	signal(SIGTERM, SigtermHandler);
	atexit(UnloadHandler);

	nexttime = time(NULL) + 1;
	while(!Fports[0].fi_shutdown) {
		int	fport;

		for(fport=0; fport < MAX_FPORTS; fport++) {
			char	xstat[64];
			int	sval;

			if(Fports[fport].fi_status == FPS_NONE)
				continue;

			xstat[0] = 0;
			sval = inp(Fports[fport].fi_port+F_RPB_STATUS_PORT);
#if	SIMINT
			if(!(sval & STAT_NOT_EMPTY)) {
				/* simulate interrupt */
				_disable();
				if(Fports[fport].fi_status == FPS_BUSY)
					FPServicePort(&Fports[fport]);
				_enable();
			}
#endif
			if(time(NULL) > nexttime) {
				if(sval & STAT_NOT_BUSY)
					strcat(xstat,"NB ");
				else
					strcat(xstat,"BY ");
				if(sval & STAT_NOT_ACK)
					strcat(xstat,"NA ");
				else
					strcat(xstat,"AK ");
				if(sval & STAT_PE)
					strcat(xstat,"PO ");
				else
					strcat(xstat,"PI ");
				if(sval & STAT_SELECT)
					strcat(xstat,"SEL ");
				else
					strcat(xstat,"NSL ");
				if(sval & STAT_NOT_ERROR)
					strcat(xstat,"NE  ");
				else
					strcat(xstat,"ERR ");
				if(sval & STAT_NOT_EMPTY)
					strcat(xstat,"FNE");
				else
					strcat(xstat,"FE ");
				ConsolePrintf("FPORT 0x%x ints %d portstatus 0x%x resets %d emptys %d dstatus %d \r\n%s\r\n",
					Fports[fport].fi_port,Fports[fport].fi_ints, sval,
					Fports[fport].fi_resets,Fports[fport].fi_emptys, Fports[fport].fi_status, xstat );
				for(sval=0; sval < Fports[fport].fi_cacheblocks; sval++)
					ConsolePrintf("%d=%d ",sval,(Fports[fport].fi_blocks+sval)->cb_type);
				ConsolePrintf("\r\n");
				nexttime = time(NULL) + WATCHDOG_UPDATE_DELAY;
			} /* end if next time */
#if     SIMINT
			ThreadSwitch();
#endif
		}	/* end for */
#if	SIMINT
		ThreadSwitch();
#else
		delay(WATCHDOG_UPDATE_DELAY*1000);
#endif
	}
	return(0);
}
