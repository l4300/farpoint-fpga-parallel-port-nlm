Sample Text Banner Page Prints all values

Userid:         \(userid                      )
Filename:       \(filename                    )
Description:    \(description                                  )
Jobnumber:      \(jobnumber)
BannerName:     \(bannername             )
BannerFileName: \(bannerfilename     )
HeaderFileName: \(headerfilename     )
Directory Path: \(dirpath                                                  )
Queue Name    : \(queuename              )
Date:           \(date      )
Time:           \(time        )
PrintServerName:\(pserver      )


End of Example output

