// testcard.cpp

#include	<stdio.h>
#include	<fstream.h>
#include	<dos.h>
#include	<stdlib.h>
#include	<conio.h>
#include	<string.h>

void	delay(unsigned);

int	icount;

void interrupt	_loadds iservice(...)
{
	icount++;
	outp(0xa0, 0x20);
	outp(0x20,0x20);
}

int
main(int argc, char *argv[])
{

	int	doconfig;
	int	x;

	if(argc < 3) {
		 cerr << "Usage: " << argv[0] << " <portnum> <doconfig>\n";
		 exit(1);
	}

	int	port;
	void	interrupt	(*oldint)(...);

	oldint = getvect(0x77);

	setvect(0x77,iservice);
	sscanf(argv[1],"%x",&port);
	sscanf(argv[2],"%d",&doconfig);
	cout << "Port is " << hex << port << dec << " doconfig " << doconfig << "\n";

	if(doconfig) {
		ifstream file("fport",ios::in|ios::binary);
		if(file.fail()) {
			cerr << "Couldn't open fport file\n";
			exit(2);
		}

		unsigned char *data = (unsigned char *) malloc(12048);
		if(!data) {
			cerr << "couldn't allocate databuf\n";
			exit(3);
		}
		file.read(data, 12048);
		if(file.gcount() != 12048) {
			cerr << "Read " << file.gcount() << " bytes from fpga file\n";
			exit(4);
		}

		outp(port+2, 8);
		delay(55);
		outp(port + 2, 0xc);

		outp(port+7, 2);
		delay(100);
		for(int x = 0; x < 12048; x++)
			outp(port+7, *(data + x));

		free(data);
		if(inp(port+1) == 0xff) {
			cerr << "FPGA Failed initialization\n";
			exit(5);
		}
		outp(port+2, 8);
		delay(110);
		outp(port + 2, 0xc);
	}
	char *teststring = "0 0 moveto 100 100 lineto stroke showpage\004";

	outp(port+2, 0x10|0x4);
	int	mask = inp(0xa1);
	cout << "oldmask " << hex << mask << "\n";

	mask &= 0x7f;
	outp(0xa1, mask);

	outp(port+6, 1);
	cout << "Pre-write status 0x" << hex << (int) inp(port+1) << "\n";

	for(x = 0; x < strlen(teststring); x++)
		outp(port + 4, *(teststring + x));

	cout << "Post-write status 0x" << hex << (int) inp(port+1) << "\n";
	delay(100);
	setvect(0x77, (void interrupt (*)(...)) oldint);
	cout << "Intcount " << icount << "\n";
	outp(0xa1, mask | 0x80);
	return(0);
}